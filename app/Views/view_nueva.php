<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        
        <title>Form Nueva Solicitud</title>
    </head>
    <body>


<h1 class="mt-5 ml-5">Form Nueva Solicitud</h1>
<h5 class="ml-5 mb-4">Por Carlos González 2ºASIR</h5>
<div class="ml-5">
<form action="<?= site_url('/nueva_solicitud/submit')?>" method="post">
            

            
            <label>ID Solicitud</label><br>
            <input name="id" size="23" type="text" disabled value="generada automáticamente"><br><br>
            
            <label>NIF</label><br>
            <input name="nif" size="23" type="text"><br><br>
                
            <label>Primer Apellido</label><br>
            <input name="apellido1" size="23" type="text"><br><br>
            
            <label>Segundo Apellido</label><br>
            <input name="apellido2" size="23" type="text"><br><br>
            
            <label>Nombre</label><br>
            <input name="nombre" size="23" type="text"><br><br>
            
            <label>Correo Electrónico</label><br>
            <input name="email" size="23" type="email"><br><br>
            
            <label>Ciclo</label><br>
            <input name="ciclo" size="23" type="text"><br><br>
            
            <!-- He intentado hacer el dropdown. He conseguido la estructura
            pero no es funcional, así que lo dejo como estaba. En la función
            nueva_solicitud_submit del controlador home explico más.
            <select name="ciclo">

                <?php foreach ($resultado_ciclos as $todos): ?>
                
                <option value="<?= $todos->nomciclo ?>"><?= $todos->nomciclo ?> </option>
                
                <?php endforeach; ?>
                
            </select>
            -->
            <br><br>
            
            <label>Tipo de tasa (codigo)</label><br>
            <input name="tipo_tasa" size="23" type="text" value="2" ><br><br>

            <input type="submit"  class="btn btn-primary mb-3" value="ENVIAR">  
            <a href="<?= site_url('/tablapau') ?>" class="btn btn-dark mb-3">VOLVER A SOLICITUDES</a>
        </form> 
 
    
</div>


    </body>
</html>