<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title>Lista Solicitudes Pau</title>
    </head>
    <body>

        <table class="table table-striped m-4">
            <h1 class='m-4'>Lista Solicitudes Pau <a href="<?= site_url("/nueva_solicitud")?>" class="btn btn-PRIMARY ml-4">NUEVA SOLICITUD</a>
            </h1>
            

                <tr>
                    <th>ID</th>
                    <th>NIF</th>
                    <th>NOMBRE</th>
                    <th>CICLO</th>
                    <th>TASA</th>
                    <th></th>
                </tr>
                <?php foreach ($resultado_select_pau as $todos): ?>
                <tr>
                    <td>
                        <?= $todos->id ?>
                    </td>
                    <td>
                        <?= $todos->nif ?>
                    </td>
                    <td>
                        <?= $todos->nombre ?>
                    </td>
                    <td>
                        <?= $todos->nomciclo ?>
                    </td>
                    <td>
                        <?php if($todos->tipo_tasa == 1): ?>
                        Ordinaria
                        <?php elseif($todos->tipo_tasa == 2): ?>
                        Semigratuita
                        <?php elseif($todos->tipo_tasa == 3): ?>
                        Gratuita
                        <?php else: ?>
                        <i>dato no encontrado</i>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?= site_url("/tablapau/".$todos->id)?>" class="btn btn-danger">BORRAR SOLICITUD</a>
                    </td>


                </tr>

                <?php endforeach; ?>
        </table>
        

    </body>
</html>