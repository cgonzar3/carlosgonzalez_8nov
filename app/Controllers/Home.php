<?php

namespace App\Controllers;

class Home extends BaseController {

    public function index() {
        return view('welcome_message');
    }

    public function test() {
        echo "<p>funciona</p>";
        echo "<p>funciona 2</p>";
    }

    public function tablapau() {

        $datospau = new \App\Models\SolicitudesPau();

        $solicitudes_pau ['resultado_select_pau'] = $datospau
                ->select('pau.id, pau.nif, pau.nombre, ciclos.nombre as "nomciclo", pau.tipo_tasa')
                ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                ->findAll();

        return view('view_solicitudes', $solicitudes_pau);
    }

    public function tablapau_borrar($id_solicitud_borrar) {
        
        $datospau = new \App\Models\SolicitudesPau();

        //borrar entrada con la IP seleccionada:
        $datospau->delete(['id' => $id_solicitud_borrar]);

        $solicitudes_pau ['resultado_select_pau'] = $datospau
                ->select('pau.id, pau.nif, pau.nombre, ciclos.nombre as "nomciclo", pau.tipo_tasa')
                ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                ->findAll();

        return view('view_solicitudes', $solicitudes_pau);
    }

    public function nueva_solicitud() {
        $datospau = new \App\Models\SolicitudesPau();

        $select_ciclos ['resultado_ciclos'] = $datospau
                ->select('ciclos.nombre as "nomciclo"')
                ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                ->findAll();
        
        return view('view_nueva',$select_ciclos);
    }

    public function nueva_solicitud_submit() {

        $datospau = new \App\Models\SolicitudesPau();

        $new_nif = $this->request->getPost('nif');
        $new_apellido1 = $this->request->getPost('apellido1');
        $new_apellido2 = $this->request->getPost('apellido2');
        $new_nombre = $this->request->getPost('nombre');
        $new_email = $this->request->getPost('email');
        $new_tipo_tasa = $this->request->getPost('tipo_tasa');
        
        //intento de obtener el id del ciclo teniendo el nombre
        //(falla por converisón del string, no he conseguido pasar la variable
        //de ciclos.nombre a ciclos.id)
        /*
        $new_ciclo = $datospau
                    ->select('pau.ciclo')
                    ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                    ->where('ciclos.nombre', $this->request->getPost('ciclo'))
                    ->findAll();
        */
        
        //al final lo dejo como estaba.
        $new_ciclo = $this->request->getPost('ciclo');
        
        $array_nueva_solicitud = [
            "nif" => $new_nif,
            "apellido1" => $new_apellido1,
            "apellido2" => $new_apellido2,
            "nombre" => $new_nombre,
            "email" => $new_email,
            "ciclo" => $new_ciclo,
            "tipo_tasa" => $new_tipo_tasa,
        ];
        
        $datospau->insert($array_nueva_solicitud);
        
        $solicitudes_pau ['resultado_select_pau'] = $datospau
                ->select('pau.id, pau.nif, pau.nombre, ciclos.nombre as "nomciclo", pau.tipo_tasa')
                ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                ->findAll();
        
        return view('view_solicitudes', $solicitudes_pau);
    }
    
    
    
    public function testcarlos() {
        $datospau = new \App\Models\SolicitudesPau();
        
        $new_ciclo ['cicloseleccionado'] = $datospau
                    ->select('pau.ciclo')
                    ->join('ciclos', 'ciclos.id = pau.ciclo', 'LEFT')
                    ->where('ciclos.nombre', $this->request->getPost('ciclo'))
                    ->findAll();
        
        
    }

}
