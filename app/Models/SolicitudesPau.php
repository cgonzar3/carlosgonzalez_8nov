<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of SolicitudesPau
 *
 * @author a023948058k
 */

namespace App\Models;

use CodeIgniter\Model;

class SolicitudesPau extends Model {

    protected $table = 'pau';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    
    protected $allowedFields = [
        'nif',
        'apellido1',
        'apellido2',
        'nombre',
        'email',
        'ciclo',
        'tipo_tasa',
    ];

}
